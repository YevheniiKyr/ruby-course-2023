class A
  def method_missing(method_name, *args, &block)
    if method_name =~ /hello\d+/
      puts "hello method definition"
    else
      super
    end
  end
end
