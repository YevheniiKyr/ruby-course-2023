require 'rackup'

class App
  def call(env)
    puts "Env: #{env}"
    if env['REQUEST_METHOD'] == 'GET' && env['REQUEST_PATH'] == '/hello'
      [
        200,
        {'Content-Type' => 'text/html'},
        ['Hello, <b>students!</b>']
      ]
    else
      [
        404,
        {'Content-Type' => 'application/json'},
        ['{"error": "Page not found"}']
      ]
    end
  end
end

Rackup::Handler.default.run App.new
